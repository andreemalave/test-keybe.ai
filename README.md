# weather app for keybe.ai test

> weather app crated with in nuxt/vue

It uses darksky.net api for weather data and google places api for locations list dropdown

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```
 
=======
# Test Keybe.Ai

A keybe.ai developer test

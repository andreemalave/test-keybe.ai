export default ({ req, store }) => {
  if (process.server) {
    const ip = req.connection.remoteAddress || req.socket.remoteAddress
    store.commit('SET_IP', ip)
  }
  if (process.client) {
    localStorage.setItem('ip', store.getters.ip)
  }
}
